module gokit2

go 1.16

require (
	github.com/go-kit/kit v0.12.0
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.1.2
	github.com/hashicorp/consul/api v1.10.1
	github.com/opentracing/opentracing-go v1.2.0
	github.com/sony/gobreaker v0.4.1
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	go.uber.org/zap v1.19.1
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
