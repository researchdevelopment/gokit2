package main

import (
	"flag"
	"fmt"
	"github.com/google/uuid"
	stdopentracing "github.com/opentracing/opentracing-go"
	"gokit2/internal/service/api/endpoints"
	"gokit2/internal/service/api/transport"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/reflection"
	"net"
	"net/http"
	"time"

	"gokit2/internal/service/api/service"
	healthgrpc "google.golang.org/grpc/health/grpc_health_v1"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kitzap "github.com/go-kit/kit/log/zap"
	consulsd "github.com/go-kit/kit/sd/consul"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	consulapi "github.com/hashicorp/consul/api"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	application = "api"
)

func main() {
	var (
		consulAddr = flag.String("consul.addr", "dcr:8500", "Consul agent address")
		httpPort   = flag.String("http.port", "8080", "HTTP server port")
		grpcPort   = flag.String("grpc.port", "50050", "GRPC server port")
	)
	flag.Parse()

	var logger log.Logger
	{
		cfg := zap.Config{
			Encoding: "json",
			//Encoding:         "console",
			Level:            zap.NewAtomicLevelAt(zapcore.DebugLevel),
			OutputPaths:      []string{"stderr"},
			ErrorOutputPaths: []string{"stderr"},
			EncoderConfig: zapcore.EncoderConfig{
				//MessageKey: "msg",

				//LevelKey:    "lvl",
				//EncodeLevel: zapcore.LowercaseLevelEncoder,

				TimeKey:    "ts",
				EncodeTime: zapcore.ISO8601TimeEncoder,

				CallerKey:    "cl",
				EncodeCaller: zapcore.ShortCallerEncoder,
			},
			InitialFields: map[string]interface{}{
				"svc": application,
			},
		}
		customLog, _ := cfg.Build()
		logger = kitzap.NewZapSugarLogger(customLog, zapcore.DebugLevel)
	}
	level.Info(logger).Log("msg", "service starting")

	errs := make(chan error, 1)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGALRM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	// Service discovery domain. In this example we use Consul.
	var client consulsd.Client
	{
		consulConfig := consulapi.DefaultConfig()
		if len(*consulAddr) > 0 {
			consulConfig.Address = *consulAddr
		}
		consulClient, err := consulapi.NewClient(consulConfig)
		if err != nil {
			logger.Log("err", err)
			os.Exit(1)
		}
		client = consulsd.NewClient(consulClient)
	}
	asr := consulapi.AgentServiceRegistration{
		ID:      uuid.New().String(),
		Name:    application,
		Address: "192.168.0.253",
		Port:    50051,
		Tags:    []string{},
		Check: &consulapi.AgentServiceCheck{
			Interval:                       (time.Duration(10) * time.Second).String(),
			GRPC:                           fmt.Sprintf("%v:%v/%v", "192.168.0.253", *grpcPort, application),
			DeregisterCriticalServiceAfter: (time.Duration(1) * time.Minute).String(),
		},
	}
	registar := consulsd.NewRegistrar(client, &asr, logger)
	registar.Register()
	defer registar.Deregister()

	//tracer := initOpentracing()

	jcfg := config.Configuration{
		ServiceName: application,
		Disabled:    false, // Nop tracer if True
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
			// Адрес рядом стоящего jaeger-agent, который будет репортить спаны
			LocalAgentHostPort: "dcr:6831",
		},
	}

	tracer, _, _ := jcfg.NewTracer(
		config.Logger(jaeger.NullLogger),
	)
	stdopentracing.SetGlobalTracer(tracer)

	service := NewServer(*consulAddr, logger)
	endpoints := endpoints.New(service, logger, tracer)

	hs := health.NewServer()
	hs.SetServingStatus(application, healthgrpc.HealthCheckResponse_SERVING)

	go startHTTPServer(endpoints, tracer, *httpPort, logger, errs)
	go startGRPCServer(endpoints, tracer, *grpcPort, hs, logger, errs)

	level.Error(logger).Log("exit", <-errs)
}

func startHTTPServer(
	endpoints endpoints.Endpoints,
	tracer stdopentracing.Tracer,
	//zipkinTracer *zipkin.Tracer,
	port string,
	logger log.Logger,
	errs chan error,
) {
	p := fmt.Sprintf(":%s", port)
	level.Info(logger).Log("protocol", "HTTP", "exposed", port)
	errs <- http.ListenAndServe(p, transports.NewHTTPHandler(endpoints, tracer, logger))
}

func startGRPCServer(
	endpoints endpoints.Endpoints,
	tracer stdopentracing.Tracer,
	//zipkinTracer *zipkin.Tracer,
	port string,
	hs *health.Server,
	logger log.Logger,
	errs chan error,
) {
	p := fmt.Sprintf(":%s", port)
	listener, err := net.Listen("tcp", p)
	if err != nil {
		level.Error(logger).Log("protocol", "GRPC", "listen", port, "err", err)
		os.Exit(1)
	}

	var server *grpc.Server
	level.Info(logger).Log("protocol", "GRPC", "protocol", "GRPC", "exposed", port)
	server = grpc.NewServer(
		grpc.UnaryInterceptor(kitgrpc.Interceptor),
	)
	healthgrpc.RegisterHealthServer(server, hs)
	reflection.Register(server)
	errs <- server.Serve(listener)
}

func NewServer(
	consulAddr string,
	logger log.Logger,
) service.ApiService {
	svc, err := service.NewCoreServiceEndpoints(consulAddr, logger)
	if err != nil {
		level.Error(logger).Log("err", err)
	}

	service := service.New(logger, svc)
	return service
}

func initOpentracing() (tracer stdopentracing.Tracer) {
	return stdopentracing.GlobalTracer()
}
