package main

import (
	"flag"
	"fmt"
	"github.com/google/uuid"
	stdopentracing "github.com/opentracing/opentracing-go"
	"gokit2/internal/service/core/endpoints"
	"gokit2/internal/service/core/transports"
	"gokit2/pkg/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/reflection"
	"net"
	"net/http"
	"time"

	"gokit2/internal/service/core/service"
	healthgrpc "google.golang.org/grpc/health/grpc_health_v1"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kitzap "github.com/go-kit/kit/log/zap"
	consulsd "github.com/go-kit/kit/sd/consul"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	consulapi "github.com/hashicorp/consul/api"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	application = "core"
)

func main() {
	var (
		consulAddr  = flag.String("consul.addr", "dcr:8500", "Consul agent address")
		httpPort    = flag.String("http.port", "8081", "HTTP server port")
		grpcPort    = flag.Int("grpc.port", 50051, "GRPC server port")
		jaegerAgent = flag.String("jaeger", "dcr:6831", "jaeger agent address")
		localIP     = flag.String("local.ip", "192.168.0.253", "local ip-address")
	)
	flag.Parse()

	var logger log.Logger
	{
		//logger = log.NewJSONLogger(os.Stdout)
		//logger = log.NewLogfmtLogger(os.Stderr)
		//logger = log.NewSyncLogger(logger)
		//logger = level.NewFilter(logger, level.AllowDebug())
		//logger = log.With(logger,
		//	"app",application,
		//	"ts", log.DefaultTimestampUTC,
		//	"clr", log.DefaultCaller,
		//)

		cfg := zap.Config{
			Encoding: "json",
			//Encoding:         "console",
			Level:            zap.NewAtomicLevelAt(zapcore.DebugLevel),
			OutputPaths:      []string{"stderr"},
			ErrorOutputPaths: []string{"stderr"},
			EncoderConfig: zapcore.EncoderConfig{
				//MessageKey: "msg",

				//LevelKey:    "lvl",
				//EncodeLevel: zapcore.LowercaseLevelEncoder,

				TimeKey:    "ts",
				EncodeTime: zapcore.ISO8601TimeEncoder,

				CallerKey:    "cl",
				EncodeCaller: zapcore.ShortCallerEncoder,
			},
			InitialFields: map[string]interface{}{
				"svc": application,
			},
		}
		customLog, _ := cfg.Build()
		logger = kitzap.NewZapSugarLogger(customLog, zapcore.DebugLevel)

		//devLog,_ := zap.NewDevelopment()
		//logger = kitzap.NewZapSugarLogger(devLog, zapcore.DebugLevel)
	}
	level.Info(logger).Log("msg", "service starting")

	errs := make(chan error, 1)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGALRM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	// Service discovery domain. In this example we use Consul.
	var client consulsd.Client
	{
		consulConfig := consulapi.DefaultConfig()
		if len(*consulAddr) > 0 {
			consulConfig.Address = *consulAddr
		}
		consulClient, err := consulapi.NewClient(consulConfig)
		if err != nil {
			logger.Log("err", err)
			os.Exit(1)
		}
		client = consulsd.NewClient(consulClient)
	}
	asr := consulapi.AgentServiceRegistration{
		ID:      uuid.New().String(),
		Name:    application,
		Address: *localIP,
		Port:    *grpcPort,
		Tags:    []string{},
		Check: &consulapi.AgentServiceCheck{
			Interval:                       (time.Duration(10) * time.Second).String(),
			GRPC:                           fmt.Sprintf("%v:%v/%v", "192.168.0.253", *grpcPort, application),
			DeregisterCriticalServiceAfter: (time.Duration(1) * time.Minute).String(),
		},
	}
	registar := consulsd.NewRegistrar(client, &asr, logger)
	registar.Register()
	defer registar.Deregister()

	//tracer := initOpentracing()

	jcfg := config.Configuration{
		ServiceName: application,
		Disabled:    false, // Nop tracer if True
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
			LocalAgentHostPort:  *jaegerAgent,
		},
	}

	tracer, _, _ := jcfg.NewTracer(
		config.Logger(jaeger.NullLogger),
	)

	stdopentracing.SetGlobalTracer(tracer)

	service := NewServer(logger)
	endpoints := endpoints.New(service, logger, stdopentracing.GlobalTracer())

	hs := health.NewServer()
	hs.SetServingStatus(application, healthgrpc.HealthCheckResponse_SERVING)

	go startHTTPServer(endpoints, tracer, *httpPort, logger, errs)
	go startGRPCServer(endpoints, tracer, *grpcPort, hs, logger, errs)

	level.Error(logger).Log("exit", <-errs)
}

func startHTTPServer(
	endpoints endpoints.Endpoints,
	tracer stdopentracing.Tracer,
	//zipkinTracer *zipkin.Tracer,
	port string,
	logger log.Logger,
	errs chan error,
) {
	p := fmt.Sprintf(":%s", port)
	level.Info(logger).Log("protocol", "HTTP", "exposed", port)
	errs <- http.ListenAndServe(p, transports.NewHTTPHandler(endpoints, tracer, logger))
}

func startGRPCServer(
	endpoints endpoints.Endpoints,
	tracer stdopentracing.Tracer,
	//zipkinTracer *zipkin.Tracer,
	port int,
	hs *health.Server,
	logger log.Logger,
	errs chan error,
) {
	p := fmt.Sprintf(":%d", port)
	listener, err := net.Listen("tcp", p)
	if err != nil {
		level.Error(logger).Log("protocol", "GRPC", "listen", port, "err", err)
		os.Exit(1)
	}

	var server *grpc.Server
	level.Info(logger).Log("protocol", "GRPC", "protocol", "GRPC", "exposed", port)
	server = grpc.NewServer(
		grpc.UnaryInterceptor(kitgrpc.Interceptor),
	)
	pb.RegisterAddsvcServer(server, transports.MakeGRPCServer(endpoints, tracer, logger))
	healthgrpc.RegisterHealthServer(server, hs)
	reflection.Register(server)
	errs <- server.Serve(listener)
}

func NewServer(logger log.Logger) service.CoreService {
	service := service.New(logger)
	return service
}

func initOpentracing() (tracer stdopentracing.Tracer) {
	return stdopentracing.GlobalTracer()
}
