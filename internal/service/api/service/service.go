package service

import (
	"context"
	"github.com/go-kit/kit/log"
	coreservice "gokit2/internal/service/core/service"
)

// Middleware describes a service (as opposed to endpoint) middleware.
type Middleware func(ApiService) ApiService

// Service describes a service that adds things together
// Implement yor service methods methods.
// e.x: Foo(ctx context.Context, s string)(rs string, err error)
type ApiService interface {
	Sum(ctx context.Context, a int64, b int64) (rs int64, err error)
	Concat(ctx context.Context, a string, b string) (rs string, err error)
}

// the concrete implementation of service interface
type stubCoreService struct {
	logger  log.Logger `json:"logger"`
	coresvc coreservice.CoreService
}

// New return a new instance of the service.
// If you want to add service middleware this is the place to put them.
func New(
	logger log.Logger,
	coresvc coreservice.CoreService,
) (s ApiService) {
	var svc ApiService
	{
		svc = &stubCoreService{
			logger:  logger,
			coresvc: coresvc,
		}
		svc = LoggingMiddleware(logger)(svc)
	}
	return svc
}

// Implement the business logic of Sum
func (ad *stubCoreService) Sum(ctx context.Context, a int64, b int64) (rs int64, err error) {
	//conn, err := grpc.Dial("192.168.0.253:50051",
	//	grpc.WithInsecure(),
	//	grpc.WithTimeout(time.Second),
	//
	//)
	//
	//if err != nil {
	//	fmt.Fprintf(os.Stderr, "error: %v", err)
	//	os.Exit(1)
	//}
	//defer conn.Close()
	//svc := transports.NewGRPCClient(conn, opentracing.GlobalTracer(), log.NewNopLogger())

	//svc, err := NewCoreServiceEndpoints("dcr:8500", ad.logger)
	//if err != nil {
	//	return 0, err
	//}
	//
	//return svc.Sum(ctx, a, b)

	//return a + b, err

	return ad.coresvc.Sum(ctx, a, b)
}

// Implement the business logic of Concat
func (ad *stubCoreService) Concat(ctx context.Context, a string, b string) (rs string, err error) {
	return a + b, err
}
