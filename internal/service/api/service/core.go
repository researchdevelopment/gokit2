package service

import (
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/consul"
	"github.com/go-kit/kit/sd/lb"
	consulapi "github.com/hashicorp/consul/api"
	"github.com/opentracing/opentracing-go"
	coreendpoints "gokit2/internal/service/core/endpoints"
	coreservice "gokit2/internal/service/core/service"
	coretransports "gokit2/internal/service/core/transports"
	"google.golang.org/grpc"
	"io"
	"time"
)

func factoryFor(makeEndpoint func(coreservice.CoreService) endpoint.Endpoint, logger log.Logger) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		conn, err := grpc.Dial(instance, grpc.WithInsecure())
		if err != nil {
			return nil, nil, err
		}
		service := coretransports.NewGRPCClient(conn, opentracing.GlobalTracer(), logger)
		endpoint := makeEndpoint(service)

		return endpoint, conn, nil
	}
}

// New returns a service that's load-balanced over instances of profilesvc found
// in the provided Consul server. The mechanism of looking up profilesvc
// instances in Consul is hard-coded into the client.
func NewCoreServiceEndpoints(consulAddr string, logger log.Logger) (coreservice.CoreService, error) {
	apiclient, err := consulapi.NewClient(&consulapi.Config{
		Address: consulAddr,
	})
	if err != nil {
		return nil, err
	}

	// As the implementer of profilesvc, we declare and enforce these
	// parameters for all of the profilesvc consumers.
	var (
		consulService = "core"
		consulTags    []string
		passingOnly   = true
		retryMax      = 2
		retryTimeout  = 50 * time.Millisecond
	)

	var (
		sdclient  = consul.NewClient(apiclient)
		instancer = consul.NewInstancer(sdclient, logger, consulService, consulTags, passingOnly)
		endpoints coreendpoints.Endpoints
	)
	{
		factory := factoryFor(coreendpoints.MakeSumEndpoint, logger)
		endpointer := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(endpointer)
		//balancer := lb.NewRandom(endpointer, time.Now().Unix())

		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SumEndpoint = retry
	}
	{
		factory := factoryFor(coreendpoints.MakeConcatEndpoint, logger)
		endpointer := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(endpointer)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ConcatEndpoint = retry
	}
	return endpoints, nil
}
